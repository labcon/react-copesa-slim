import React from "react"

import Layout from "../components/layout/layout"
import SEO from "../components/seo/seo"

import Hero from "../components/hero/hero"
// import Glitch from '../components/glitch/glitch'

const IndexPage = () => (
  <Layout>
    <SEO
      title="Rastros"
      keywords={[
        `rastros`,
        `reportajes`,
        `latercera`,
        `lab`,
        `laboratoriodecontenidos`,
        `privacidad`,
      ]}
    />
    <Hero />
  </Layout>
)

export default IndexPage
