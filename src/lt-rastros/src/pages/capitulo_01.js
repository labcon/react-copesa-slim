import React from "react"
// import { graphql } from "gatsby";
// import { Link } from 'gatsby'

import Layout from "../components/layout"
import SEO from "../components/seo"
import Block from "../components/Block"
import Jumbo from "../components/Jumbo"
// import SpecialBlock from "../components/SpecialBlock";
// import SpecialJumbo from '../components/SpecialJumbo'
// import Image from "../components/Image";
import Stories from "../components/Stories"

import Footer from "../components/Footer"

import logo from "../images/rastros_logo.svg"
import CTAcap from "../images/capitulos_cta.svg"
import CTAstart from "../images/empezar_cta.svg"
import libro from "../images/libro.svg"
import styled from "styled-components"
import { Link } from "gatsby"

const IconBook = styled.img`
  display: inline-flex;
  vertical-align: center;
  height: 1.5rem;
  width: 1.5rem;
  transform: translateY(-0.2rem);
`

const CTAwrapper = styled.div`
  display: flex;
  flex-direction: row;
  /* border: 1px solid blue; */
  width: 80%;
  justify-content: space-between;
  a {
    width: 50%;
  }
`

const IndexPage = ({ data }) => (
  <Layout>
    <SEO
      title="Rastros"
      keywords={[`la tercera`, `especiales la tercera`, `stories`, `privacy`]}
    />
    <Block>
      <Stories />
    </Block>
  </Layout>
)
export default IndexPage
