import React from "react"
import { Link } from "gatsby"

import Stories from '../components/stories/stories'
import Layout from "../components/layout/layout"
import SEO from "../components/seo/seo"

const SecondPage = () => (
  <Layout>
    <SEO title="Episodios" />
    <Stories />
    <Link to="/">volver</Link>
  </Layout>
)

export default SecondPage
