export default {
  accent: '#a21521',
  background: 'black',
  color: 'white',
  complementary: '#b4b4b4',
  fontbody: '"Open Sans", -apple-system, BlinkMacSystemFont, avenir next, avenir, helvetica neue, helvetica, ubuntu, roboto, noto, segoe ui, arial, sans-serif',
  fonttitle:  '"Montserrat", -apple-system, BlinkMacSystemFont, avenir next, avenir, helvetica neue, helvetica, ubuntu, roboto, noto, segoe ui, arial, sans-serif',
  maxWidth: '1200px',
  button: {
    backgroundColor: '#a21521',
    color: 'white',
    borderRadius: '4px',
    padding: '1rem 2rem'
  }
};