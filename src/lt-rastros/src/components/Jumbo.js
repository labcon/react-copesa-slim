import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Jumbo = ({ children }) => (
  <div className="jumbowrapper">
    <JumboInnerWrapper>{children}</JumboInnerWrapper>
  </div>
);

const JumboInnerWrapper = styled.div`
  display: block;
`;

Jumbo.propTypes = {
  children: PropTypes.node.isRequired
};

export default Jumbo;
