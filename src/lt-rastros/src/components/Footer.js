import React from "react";

const Footer = () => (
  <footer className="footerlt">
    © Rastros {new Date().getFullYear()} La Tercera. Todos los derechos
    reservados.
  </footer>
);

export default Footer;
