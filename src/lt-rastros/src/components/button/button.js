import React from "react"
import { Link } from "gatsby"
import StyledButton from "./button.css"

const Button = ({ to, children }) => (
  <StyledButton>
    <Link to={to}>{children}</Link>
  </StyledButton>
)

export default Button
