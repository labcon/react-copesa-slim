import styled from 'styled-components'
import theme from '../../styles/theme'

const StyledButton = styled.span`
  a{
    transition: all 0.2s ease;
    text-decoration: none;
    background-color: ${theme.button.backgroundColor};
    color: ${theme.button.color};
    padding: ${theme.button.padding};
    border-radius: ${theme.button.borderRadius};
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: center;
    &:hover{
      color: ${theme.button.backgroundColor};
      background-color: ${theme.button.color};
    }
  }
`

export default StyledButton