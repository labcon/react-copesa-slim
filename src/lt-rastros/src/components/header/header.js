import React from "react"
import propTypes from "prop-types"
import { Link } from "gatsby"
import { Container } from "./header.css"
import Nav from "components/header/nav/nav"


const Header = ({ title }) => (
  <Container>
    <Link to="/">
      {/* <Title as="h1">{title}</Title> */}
    </Link>
    <Nav />
  </Container>
)

Header.propTypes = {
  title: propTypes.string.isRequired,
}

export default Header
