import styled from 'styled-components'

import Menu from './Menu'
import Logo from './Logo'
import User from './User'
import Social from './Social'
import DigitalPaper from './DigitalPaper'

const Header = styled.div`
  padding: 1rem;
`

Header.Menu = Menu
Header.Logo = Logo
Header.User = User
Header.Social = Social
Header.DigitalPaper = DigitalPaper

export default Header