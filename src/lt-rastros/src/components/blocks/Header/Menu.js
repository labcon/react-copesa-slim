import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Menu = () => (
  <div>
    <FontAwesomeIcon icon="bars" />
  </div>
)

export default Menu