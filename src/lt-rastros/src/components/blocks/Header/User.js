import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const User = () => (
  <div>
    <FontAwesomeIcon icon="user" />
  </div>

)

export default User