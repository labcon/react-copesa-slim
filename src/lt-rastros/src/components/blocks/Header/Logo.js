import React, {Component} from 'react'
import propTypes from 'prop-types'
import Svgs from 'copesa-logos'
class Logo extends Component{
  render () {
    const LogoRender = Svgs[this.props.Brand]
    return  <LogoRender />
  }
}

Logo.propTypes = {
  brand: propTypes.oneOf([
    'Latercera',
    'Beethoven',
    'Biut',
    'Diarioconcepcion',
    'Duna',
    'Glamorama',
    'Grupocopesa',
    'GrupocopesaAlt',
    'Icarito',
    'LaboratorioDeContenidosDeMarca',
    'Lacuarta',
    'LahoraTubuenanoticia',
    'LtEldeportivo',
    'LtFinde',
    'LtNegocios',
    'LtPulso',
    'LtReportajes',
    'LtTendencias',
    'Masdeco',
    'MediosDigitalesGrupocopesa',
    'Mouse',
    'Mtonline',
    'Paula',
    'Quepasa',
    'Radiodisney',
    'Zero',
    'ZeroAlt'
  ])
}

Logo.defaultProps = {
  Brand: 'Latercera'
}

export default Logo