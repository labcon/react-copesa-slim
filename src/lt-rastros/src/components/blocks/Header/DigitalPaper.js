import React from 'react'

const DigitalPaper = () => (
  <div className="digital-paper">
    <a href="http://edition.pagesuite.com/launch.aspx?pbid=b56048aa-4157-4754-8bff-0c731058b546">
      <div className="col1">
        <h1 className="ea-log">
          Descarga la<br />nueva App<br />
          <img src="https://s1.latercera.com/wp-content/uploads/2019/02/early-access.png" alt="Early Access" className="img-responsive" />
        </h1>
      </div>
      <div className="col2">
        <div className="ea-app">
          <img src="https://s1.latercera.com/wp-content/uploads/2019/02/early-access-app2.png" alt="App" />
        </div>
        <div className="over-shadow"></div>
        <div className="news">
          <img src="https://edition.pagesuite-professional.co.uk/get_image.aspx?w=300&amp;pbid=33084897-397a-48cc-b3c0-3ce1ec447137&amp;pnum=01&amp;nocache=1902201919" alt="Papel Digital" />
        </div>
      </div>
    </a>
  </div>
)

export default DigitalPaper