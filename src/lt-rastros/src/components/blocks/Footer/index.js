import styled from 'styled-components'
import { Latercera } from "copesa-logos"

const StyledFooter = styled.div`
  padding: 1rem;
`

const Footer = () => (
  <StyledFooter>
    <Latercera />
  </StyledFooter>

)

export default Footer