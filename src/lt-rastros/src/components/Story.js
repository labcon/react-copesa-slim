import React from "react";
import PropTypes from "prop-types";
import HeaderStory from "./HeaderStory";
import ReactPlayer from "react-player";
import styled from "styled-components";

export default class Story extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.story !== prevProps.story) {
      this.pauseId && clearTimeout(this.pauseId);
      this.pauseId = setTimeout(() => {
        this.setState({ loaded: false });
      }, 300);
      this.props.action("pause");
    }
  }
  goToStory() {
    console.log("go TO STORY");
  }

  imageLoaded = () => {
    if (this.pauseId) clearTimeout(this.pauseId);
    this.setState({ loaded: true });
    this.props.action("play");
  };

  render() {
    let source =
      typeof this.props.story === "object"
        ? this.props.story.url
        : this.props.story;

    let isHeader =
      typeof this.props.story === "object" && this.props.story.header;

    let isVideo =
      typeof this.props.story === "object" && this.props.story.type === "video";

    let isInteractive =
      typeof this.props.story === "object" &&
      this.props.story.type === "interactive";

    return (
      <div
        style={{
          ...styles.story,
          width: this.props.width,
          height: this.props.height
        }}
      >
        {!isInteractive && !isVideo && (
          <img
            alt=""
            style={styles.storyContent}
            src={source}
            onLoad={this.imageLoaded}
          />
        )}
        {isVideo && (
          <ReactPlayer
            url={this.props.story.url}
            playing={true}
            onReady={this.imageLoaded}
          />
        )}
        {isInteractive && (
          <ContainerInteractive>
            <QuestionInteractive>
              Aca deberia el dato de la respuesta{this.props.story.url.question}
            </QuestionInteractive>

            <AnswerInteractive
            // onClick={this.goToStory(this.props.story.target)}
            >
              aca las pregunta
            </AnswerInteractive>
            <img
              alt=""
              style={styles.storyContent}
              src={source}
              onLoad={this.imageLoaded}
            />
          </ContainerInteractive>
        )}

        {isHeader && (
          <div style={{ position: "absolute", left: 12, top: 20 }}>
            {this.props.header ? (
              () => this.props.header(this.props.story.header)
            ) : (
              <HeaderStory
                heading={this.props.story.header.heading}
                subheading={this.props.story.header.subheading}
                profileImage={this.props.story.header.profileImage}
              />
            )}
          </div>
        )}
        {!this.state.loaded && (
          <div
            style={{
              width: this.props.width,
              height: this.props.height,
              position: "absolute",
              left: 0,
              top: 0,
              background: "rgba(0, 0, 0, 0.9)",
              zIndex: 9,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              color: "#ccc"
            }}
          >
            {this.props.loader || `Cargando..`}
          </div>
        )}
      </div>
    );
  }
}
const QuestionInteractive = styled.div`
  align-self: center;
  border: 1px solid blue;
  width: 100%;
  height: 100%;
  text-align: center;
  font-family: Arial, Helvetica, sans-serif;
  color: white;
`;

const AnswerInteractive = styled.a`
  align-self: center;
  border: 1px solid blue;
  width: 100%;
  height: 100%;
  text-align: center;
  font-family: Arial, Helvetica, sans-serif;
  color: white;
`;

const ContainerInteractive = styled.div`
  width: ${Story.width};
  height: 100%;
  background-color: red;
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: center;
  border: 1px solid green;
`;

const styles = {
  story: {
    display: "flex"
  },
  storyContent: {
    width: "auto",
    maxWidth: "100%",
    maxHeight: "100%",
    margin: "auto"
  }
};

Story.propTypes = {
  story: PropTypes.object || PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number,
  action: PropTypes.func,
  loader: PropTypes.element,
  header: PropTypes.element,
  type: PropTypes.string,
  destiny: PropTypes.string || PropTypes.number
};
