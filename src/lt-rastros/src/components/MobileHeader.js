import PropTypes from "prop-types";
import React from "react";
// import Header from './blocks/Header'
import styled from "styled-components";
import { LtTendencias } from "copesa-logos";

const SiteHeader = () => (
  <header className="mobileheader">
    <MainLogoLink href="https://www.latercera.com">
      <LtTendencias />
    </MainLogoLink>
  </header>
);

const MainLogoLink = styled.a`
  max-width: 200px;
  max-height: 40px;
  display: block;
  margin: 20px auto;
  svg {
    display: block;
    width: 100%;
  }
`;

SiteHeader.propTypes = {
  siteTitle: PropTypes.string
};

SiteHeader.defaultProps = {
  siteTitle: ``
};

export default SiteHeader;
