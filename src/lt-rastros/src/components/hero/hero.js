import React from "react"

import { Container, Content, Col1, Ctawrapper, Icon, Bg } from "./hero.css"
import Button from "../button/button"
import Image from "../image/image"
import logo from "../../images/logo-rastros.svg"
import iconBook from "../../images/icon-book.svg"

const Hero = () => (
  <Container>
    <Image filename="home-eye.jpg" bg />
    <Bg />
    <Content>
      <Col1>
        <img src={logo} alt="Rastros" />
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
          fermentum vestibulum nisi id pretium. Aenean aliquam leo at ligula
          iaculis pulvinar.
        </p>
        <Ctawrapper>
          <Button> Episodios </Button>
          <Button to="/capitulo_01"> Empezar </Button>
        </Ctawrapper>
        <Button to="/reportaje_cap01">
          <Icon src={iconBook} alt="" />
          <span> Leer la investigación completa</span>
        </Button>
      </Col1>
    </Content>
  </Container>
)

export default Hero
