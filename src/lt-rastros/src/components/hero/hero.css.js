import styled from 'styled-components'
import theme from '../../styles/theme'

const Container = styled.section`
  position: relative;
  padding: 1rem;
`

const Content = styled.div`
  display: grid;
  justify-content: space-between;
  align-items: center;
  grid-template-columns: repeat(2, 1fr);
  min-height: 80vh;
  max-width: ${theme.maxWidth};
  margin-left: auto;
  margin-right: auto;
`;

const Col1 = styled.div`
  display: block;
  letter-spacing: 1px;
`

const Ctawrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 1rem;
  margin-bottom: 1rem;
  a{
    display: block;
    background-color: ${theme.accent};
    color: white;
    padding: ${theme.button.padding};
    border-radius: ${theme.button.borderRadius};
    text-align: center;
  }
`

const Bg = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;
  background: linear-gradient(to right,rgba(0,0,0,1) 0%,rgba(0,0,0,0) 60%), linear-gradient(to top, rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 60%);
`

const Icon = styled.img`
  display: inline-flex;
  vertical-align: center;
  height: 1.5rem;
  width: 1.5rem;
  padding-right: 0.5rem;
`

export { Container, Content, Col1, Ctawrapper, Icon, Bg }