import React from "react"
import {ParallaxProvider, Parallax} from 'react-scroll-parallax'

const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const V1 = {
  bounds: [130, 120],
  forms: [
    <svg viewBox="0 0 130 180" version="1.1">
      <polygon className="fill-5" points="64.5 179 32.25 89.5 0 0 64.5 0 129 0 96.75 89.5 64.5 179" />
    </svg>,
    <svg viewBox="0 0 130 180" version="1.1">
      <rect className="fill-1" x="45.5" y="14" width="38" height="90" rx="19" ry="19" />
    </svg>,
  ]
};

const E2 = {
  bounds: [130, 120],
  forms: [
    <svg viewBox="0 0 130 180" version="1.1">
      <polygon className="fill-4" points="0 0 129 180 0 180" />
    </svg>,
    <svg viewBox="0 0 130 180" version="1.1">
      <rect className="fill-1" width="129" height="38" rx="19" ry="19" />
      <rect className="fill-1" y="71" width="129" height="38" rx="19" ry="19" />
      <rect className="fill-1" y="142" width="129" height="38" rx="19" ry="19" />
    </svg>,
  ],
};

const R3 = {
  bounds: [130, 120],
  forms: [
    <svg viewBox="0 0 130 180" version="1.1">
      <rect className="fill-1" width="38" height="179.29" rx="19" ry="19" />
    </svg>,
    <svg viewBox="0 0 130 180" version="1.1">
      <polygon className="fill-6" points="20 54.64 109.58 179.64 20 179.64" />
    </svg>,
    <svg viewBox="0 0 130 180" version="1.1">
      <circle className="fill-2" cx="74.5" cy="56.14" r="55.5" />
    </svg>,
  ]
};

const A4 = {
  bounds: [130, 120],
  forms: [
    <svg viewBox="0 0 130 180" version="1.1">
      <polygon className="fill-5" points="64.5 0 96.75 89.5 129 179 64.5 179 0 179 32.25 89.5 64.5 0" />
    </svg>,
    <svg viewBox="0 0 130 180" version="1.1">
      <rect className="fill-1" x="45.5" y="75" width="38" height="90" rx="19" ry="19" />
    </svg>,
  ]
};

const N5 = {
  bounds: [130, 120],
  forms: [
    <svg viewBox="0 0 130 180" version="1.1">
      <rect className="fill-1" x="92" width="38" height="179.29" rx="19" ry="19" />
    </svg>,
    <svg viewBox="0 0 130 180" version="1.1">
      <polyline className="fill-4" points="20 0.64 109.58 179.64 20 179.64" />
    </svg>
  ],
};

const O6 = {
  bounds: [130, 120],
  forms: [
    <svg viewBox="0 0 130 180" version="1.1">
      <circle className="fill-2" cx="63.5" cy="63.5" r="63.5" />
    </svg>,
    <svg viewBox="0 0 130 180" version="1.1">
      <rect className="fill-1" x="44.5" y="18.5" width="38" height="90" rx="19" ry="19" />
    </svg>
  ],
};

const Gradients = () => (
  <svg width="50" height="50" version="1.1" className="hidden">
    <defs>
      <linearGradient id="gradient-1" x1="0" x2="0" y1="0" y2="1">
        <stop offset="0%" stopColor="#6ED0DD" />
        <stop offset="100%" stopColor="#70E2B9" />
      </linearGradient>
      <linearGradient id="gradient-2" x1="0" x2="0" y1="0" y2="1">
        <stop offset="0%" stopColor="#405D86" />
        <stop offset="100%" stopColor="#384257" />
      </linearGradient>
      <linearGradient id="gradient-3" x1="0" x2="0" y1="0" y2="1">
        <stop offset="0%" stopColor="#ED6088" />
        <stop offset="100%" stopColor="#C86FA3" />
      </linearGradient>
      <linearGradient id="gradient-4" x1="0" x2="0" y1="0" y2="1">
        <stop offset="0%" stopColor="#F07F6B" />
        <stop offset="100%" stopColor="#EFC15C" />
      </linearGradient>
      <linearGradient id="gradient-5" x1="0" x2="0" y1="0" y2="1">
        <stop offset="0%" stopColor="#8D63B1" />
        <stop offset="100%" stopColor="#8179CB" />
      </linearGradient>
      <linearGradient id="gradient-6" x1="0" x2="0" y1="0" y2="1">
        <stop offset="0%" stopColor="#EDD460" />
        <stop offset="100%" stopColor="#EDBC39" />
      </linearGradient>
    </defs>
  </svg>
);

const word = [V1, E2, R3, A4, N5, O6];

class Letter extends React.Component {
  render() {
    const { letter } = this.props;
    const offset = getRandomInt(100, 150);
    const isSlower = getRandomInt(0, 1) ? true : false;
    const wwidth = typeof window !== 'undefined' && window.innerWidth;
    const lheight = letter.bounds[1]
    return (
      <div className="letter" style={{
        width: '100%',
        height: lheight,
      }}>
        {letter.forms.map((X, i) =>
          <Parallax
            className="form"
            key={i}
            offsetYMin={-offset * (i + 1) + 'px'}
            offsetYMax={offset * (i + 1) + 'px'}
            slowerScrollRate={isSlower}
          >
            {X}
          </Parallax>
        )}
      </div>
    );
  }
}

const ParallaxWord = () => (
  <div className="word">
    {word.map((X, i) =>
      <Letter key={i} letter={X} />
    )}
  </div>
)

const Verano = () => (
  <ParallaxProvider>
    <main>
      <Gradients />
      <ParallaxWord />
    </main>
  </ParallaxProvider>

)

export default Verano

