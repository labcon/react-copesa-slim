import React from "react"
import SiteHeader from './SiteHeader'
import MenuHeader from './MenuHeader'
import MobileHeader from './MobileHeader'

const Header = () => (
  <header>
    <div className="header-wrap">
      <SiteHeader />
    </div>
    <div className="menu-wrap">
      <MenuHeader />
    </div>
    <div className="mobile-wrap">
      <MobileHeader />
    </div>
  </header>
)

export default Header

