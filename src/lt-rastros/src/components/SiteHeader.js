import PropTypes from "prop-types"
import React from "react"
import Header from './blocks/Header'
import styled from 'styled-components'

const SiteHeader = () => (
  <header>
    <Grid>
      <Justifier>
        <Header.Social />
      </Justifier>

      <MainLogoLink href="https://www.latercera.com">
        <Header.Logo />
      </MainLogoLink>

      <Justifier end="{true}">
        <Header.DigitalPaper />
      </Justifier>
    </Grid>
  </header>
)

const Justifier = styled.div`
  display: flex;
  justify-content: ${props => props.end ? "flex-end" : "flex-start"};
`

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 1rem;
  align-items: flex-end;
  padding: 1rem;
`

const MainLogoLink = styled.a`
  max-width: 300px;
  max-height: 77px;
  display: block;
  margin: 25px auto 0;
  svg{
    display: block;
    width: 100%;
  }
`

SiteHeader.propTypes = {
  siteTitle: PropTypes.string,
}

SiteHeader.defaultProps = {
  siteTitle: ``,
}

export default SiteHeader
