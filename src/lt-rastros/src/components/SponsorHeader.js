import PropTypes from 'prop-types'
import React from 'react'
import Header from './blocks/Header'
import styled from 'styled-components'

const Justifier = styled.div`
  display: flex;
  justify-content: ${props => props.end ? "flex-end" : "flex-start"};
`

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 1rem;
  align-items: center;
  padding: 1rem;
`

const MainLogoLink = styled.a`
  max-width: 200px;
  max-height: 60px;
  display: block;
  margin: 0 auto;
  svg{
    display: block;
    width: 100%;
  }
`

const SponsorHeader = () => (
  <header>
    <Grid>
      <Justifier>
        <Header.Menu />
      </Justifier>

      <MainLogoLink href="https://www.latercera.com">
        <Header.Logo />
      </MainLogoLink>

      <Justifier end>
        <Header.User />
      </Justifier>
    </Grid>
  </header>
)


SponsorHeader.propTypes = {
  siteTitle: PropTypes.string,
}

SponsorHeader.defaultProps = {
  siteTitle: ``,
}

export default SponsorHeader
