import React, { Component } from "react"
import Container from "./Container"
import screen0 from "../images/screen0.png"
import screen01 from "../images/screen01.png"
import screen02 from "../images/screen02.png"
import screen031 from "../images/screen031.png"
import screen032 from "../images/screen032.png"
import screen04 from "../images/screen04.png"
import screen05 from "../images/screen05.png"
import screen06 from "../images/screen06.png"
import screen07 from "../images/screen07.png"
import screen08 from "../images/screen08.png"
import screen083 from "../images/screen083.png"
import screen09 from "../images/screen09.png"
import screen10 from "../images/screen10.png"
import screen11 from "../images/screen11.png"
import perritosVideo from "../images/perritos.mp4"
import gatitosVideo from "../images/gatitos.mp4"
// import gatitosVideo from "../images/intro_cap1.mp4";

class InstaStories extends Component {
  render() {
    return (
      <Container
        stories={stories}
        defaultInterval={1500}
        loader={<div>Cargando...</div>}
        width={432}
        height={768}
      />
    )
  }
}

// {
//   // destiny: 5,
//   // history: 1,
//   url: screen01,
//   type: "interactive",
//   duration: 1000,
//   headline: "Soy un headline",
//   options: {
//     answer: {
//       text: "opcion 1",
//       target: 3
//     },
//     answer: {
//       text: "opcion 2",
//       target: 2
//     }
//   }
// },

// the params history and destiny define the target of the current story..
const stories = [
  { url: screen0 },
  { url: screen01, destiny: 5 },
  { url: screen02 },
  { url: screen031 },
  { url: screen032 },
  { url: screen04 },
  { url: screen05 },
  { url: screen06 },
  { url: screen07 },
  { url: screen08 },
  { url: perritosVideo, type: "video", duration: 20000 },
  { url: screen083 },
  { url: screen09 },
  { url: screen10 },
  { url: screen11 },
]

export default InstaStories
