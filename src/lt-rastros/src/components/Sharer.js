import React from 'react'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  FacebookShareButton,
  TwitterShareButton,
  WhatsappShareButton
} from 'react-share'

import './Share.scss'

const Share = ({ socialConfig, tags }) => (
  <div className="post-social">
    <FacebookShareButton url={socialConfig.config.url} quote={socialConfig.config.title} className="button is-rounded">
      <span className="icon">
        <FontAwesomeIcon icon={['fab', 'facebook-f']} />
      </span>
    </FacebookShareButton>
    <TwitterShareButton url={socialConfig.config.url} title={socialConfig.config.title} className="button is-rounded">
      <span className="icon">
        <FontAwesomeIcon icon={['fab', 'twitter']} />
      </span>
    </TwitterShareButton>
    <WhatsappShareButton url={socialConfig.config.url} title={socialConfig.config.title} className="button is-rounded">
      <span className="icon">
        <FontAwesomeIcon icon={['fab', 'whatsapp']} />
      </span>
    </WhatsappShareButton>
  </div>
)

Share.propTypes = {
  socialConfig: PropTypes.shape({
    twitterHandle: PropTypes.string.isRequired,
    config: PropTypes.shape({
      url: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    }),
  }).isRequired,
  tags: PropTypes.arrayOf(PropTypes.string)
}
Share.defaultProps = {
  tags: []
}

export default Share