import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #111;
  position: relative;
`

const Overlay = styled.div`
  position: absolute;
  height: inherit;
  width: inherit;
  display: flex;
`

const Left = styled.div`

`

const Right = styled.div`

`

const StyledProgressArray = styled.div`
  display: flex;
  justify-content: center;
  max-width: 100%;
  flex-wrap: row;
  position: absolute;
  width: 98%;
  padding: 5px;
  align-self: center;
  z-index: 99;
  filter: drop-shadow(0 1px 8px #000);
`

const SSContainer = styled.div`
  position: relative;
`

const SSBackground = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;
`

const SSInterface = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
`

const SSLoading = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0, 0, 0, 0.9);
  z-index: 9;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #ccc;
`

export { Container, Overlay, Left, Right, StyledProgressArray, SSContainer, SSBackground, SSInterface, SSLoading }
