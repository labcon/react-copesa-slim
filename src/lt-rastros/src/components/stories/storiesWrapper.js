import React, { Component } from 'react'
import Story from './story'
import ProgressArray from './progressArray'
import PropTypes from 'prop-types'
import { Container, Overlay } from './stories.css'

class StoriesWrapper extends Component {
  constructor(props) {
    super(props)
    this.state = {
      // currentId: -1,
      currentId: 0,
      pause: true,
      count: 0,
      storiesDone: 0
    }
    this.defaultInterval = 1200
    this.width = props.width || 720
    this.height = props.height || 1280
  }

  componentDidMount() {
    this.props.defaultInterval &&
      (
        this.defaultInterval = this.props.defaultInterval
      )
    this.start()
  }

  start = async () => {
    const stories = this.props.stories
    console.log('init start', this.state.currentId, stories.length)

    //TODO: refaccionar con reduce
    while (this.state.currentId < stories.length - 1) {
      console.log('MENOR', this.state.currentId, stories.length - 1)
      const currentId = this.state.currentId + 1
      const interval = stories[currentId].duration ? stories[currentId].duration : this.defaultInterval
      console.log(currentId, interval)

      this.setState({ currentId: currentId })

      await this.wait(interval).then(() => {
        console.log('es menor', this.state.currentId < this.props.stories.length - 1)
        this.state.currentId < this.props.stories.length - 1 &&
          this.setState({ count: 0 })
      })
    }
  }

  wait = time => {
    return new Promise(resolve => {
      this.setState({ count: 0 })
      let id = setInterval(() => {

        if (this.state.count < time) {
          if (!this.state.pause) {
            this.setState({ count: this.state.count + 1 })
          }
        } else {
          clearInterval(id)
          resolve()
        }
      }, 1)
    })
  }

  pause = action => {
    this.setState({ pause: action === 'pause' })
  }

  returnCount = () => {
    console.log('Count:', this.state.currentId)
  }

  previous = () => {
    if (this.state.currentId > 0) {
      if (this.props.stories[this.state.currentId].history) {
        this.goTo(this.props.stories[this.state.currentId].history)
      } else {
        let historyId = this.state.currentId
        this.setState(
          {
            currentId:
              historyId === this.props.stories.length - 1
                ? this.state.currentId - 2
                : this.state.currentId - 1,
            count: 0
          },
          () => {
            if (historyId === this.props.stories.length - 1) {
              this.start()
            }
          }
        )
      }
    }
  }

  next = () => {
    if (this.state.currentId < this.props.stories.length - 1) {
      if (this.props.stories[this.state.currentId].destiny) {
        this.goTo(this.props.stories[this.state.currentId].destiny)
      } else {
        this.setState({
          currentId: this.state.currentId + 1,
          count: 0
        })
      }
    } else {
      this.endOfStories()
    }
  }

  endOfStories = () => {
    console.log('final')
  }

  goTo = target => {
    console.log('goTo ', target)
    this.setState({
      currentId: target,
      count: 0
    })
  }

  debouncePause = e => {
    e.preventDefault()
    this.mousedownId = setTimeout(() => {
      this.pause('pause')
    }, 200)
  }

  mouseUp = (e, type) => {
    console.log('mouseUp', type)
    e.preventDefault()
    this.mousedownId && clearTimeout(this.mousedownId)
    if (this.state.pause) {
      this.pause('play')
    } else {
      type === 'next' ? this.next() : this.previous()
    }
  }

  render() {


    return (
      <Container>
        {this.returnCount()}
        <ProgressArray
          largo={this.props.stories.map((s, i) => i)}
          progress={{
            id: this.state.currentId,
            completed:
              this.state.count /
              ((this.props.stories[this.state.currentId] &&
                this.props.stories[this.state.currentId].duration) ||
                this.defaultInterval)
          }}
        />
        <Story
          action={this.pause}
          height={this.height}
          width={this.width}
          story={this.props.stories[this.state.currentId]}
          loader={this.props.loader}
          header={this.props.header}
        />
        <Overlay>
          <div
            style={{ width: this.width / 2, zIndex: 999 }}
            onTouchStart={this.debouncePause}
            onTouchEnd={e => this.mouseUp(e, 'previous')}
            onMouseDown={this.debouncePause}
            onMouseUp={e => this.mouseUp(e, 'previous')}
          />
          <div
            style={{ width: this.width / 2, zIndex: 999 }}
            onTouchStart={this.debouncePause}
            onTouchEnd={e => this.mouseUp(e, 'next')}
            onMouseDown={this.debouncePause}
            onMouseUp={e => this.mouseUp(e, 'next')}
          />
        </Overlay>
      </Container>
    )
  }
}

StoriesWrapper.propTypes = {
  stories: PropTypes.array,
  defaultInterval: PropTypes.number,
  width: PropTypes.number,
  height: PropTypes.number,
  loader: PropTypes.element,
  header: PropTypes.element,
  type: PropTypes.string
}

export default StoriesWrapper