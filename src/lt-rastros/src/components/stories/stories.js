import React, { Component } from 'react'
import StoriesWrapper from './storiesWrapper'

class Stories extends Component {
  render() {
    return (
      <StoriesWrapper
        stories={stories}
        defaultInterval={1500}
        loader={<div>Cargando...</div>}
      />
    )
  }
}

/* Stories
  props:

  duration
  storytype
    image
      url
    video
      url
    color
      cssProperty

  effects ?

  overlay
    text
      title
      content
    interactive
      question
      answers

*/

const stories = [
  {
    duration: 1000,
    image: {
      url: "https://www.fillmurray.com/720/1280",
    },
    interface: {
      text: {
        title: 'Bill Murray',
        content: 'Ut et nulla in incididunt in quis.'
      }
    }
  },
  {
    duration: 2500,
    image: {
      url: 'https://www.fillmurray.com/1280/1280',
    },
    interface: {
      question: {
        question: '¿Bill Murray?',
        answers: [
          {
            text: 'Sí, Bill Murray :o',
            value: '1',
          },
          {
            text: 'Nope, Bill Murray no :/',
            value: '2',
          }
        ]
      }
    }
  }
]

export default Stories