import React from 'react'
import PropTypes from 'prop-types'

const Progress = (props) => (
  <div style={{ ...styles.progress, ...{ width: `${props.width * 100}%` } }}>
    <div style={{ ...styles.overlay, width: `${props.completed * 100}%` }} />
  </div>
)

const styles = {
  progress: {
    height: 2,
    maxWidth: '100%',
    background: '#555',
    margin: 2
  },
  overlay: {
    height: '100%',
    background: 'white'
  }
}

Progress.propTypes = {
  width: PropTypes.number,
  completed: PropTypes.number
}

export default Progress