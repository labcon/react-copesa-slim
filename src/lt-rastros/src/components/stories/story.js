import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { SSContainer, SSBackground, SSInterface, SSLoading } from './stories.css'
import StoryTypeImage from './storyTypeImage'
import StoryTypeVideo from './storyTypeVideo'
import InteractiveTypeText from './interactiveTypeText'
import InteractiveTypeQuestion from './interactiveTypeQuestion'

class Story extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loaded: false
    }
  }

  componentDidUpdate(prevProps) {
    if(this.props.story !== prevProps.story) {
      this.pauseId && clearTimeout(this.pauseId)
      this.pauseId = setTimeout(() => {
        this.setState({
          loaded: false
        })
      }, 300)
      this.props.action('pause')
    }
  }

  goToStory() {
    console.log('gotoStory')
  }

  render() {
    const story = this.props.story
    console.log(story)
    const isVideo = typeof story.video !== 'undefined'
    const isImage = typeof story.image !== 'undefined'
    const isText = typeof story.interface === 'object' && typeof story.interface.text === 'object'
    const isQuestion = typeof story.interface === 'object' && typeof story.interface.question === 'object'

    return (
      <SSContainer>
        <SSBackground>
          {isVideo && (
            <StoryTypeVideo url={story.video} />
          )}
          {isImage && (
            <StoryTypeImage url={story.image} />
          )}
        </SSBackground>
        <SSInterface>
          {isText && (
            <InteractiveTypeText content={story.interface.text} />
          )}
          {isQuestion && (
            <InteractiveTypeQuestion content={story.interface.question} />
          )}
        </SSInterface>

        {!this.state.loaded && (
          <SSLoading>
            {this.props.loader || `Cargando..`}
          </SSLoading>
        )}
      </SSContainer>
    );
  }
}

Story.propTypes = {
  story: PropTypes.object,
  height: PropTypes.number,
  width: PropTypes.number,
  action: PropTypes.func,
  loader: PropTypes.element,
  header: PropTypes.element,
  destiny: PropTypes.string || PropTypes.number
};

export default Story
