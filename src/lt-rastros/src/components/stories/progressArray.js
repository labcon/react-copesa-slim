import React from 'react'
import PropTypes from 'prop-types'
import Progress from './progress'
import { StyledProgressArray } from './stories.css'

const ProgressArray = (props) => (
  <StyledProgressArray>
    {
      props.largo.map(i => (
        <Progress
          key={i}
          width={1 / props.largo.length}
          completed={
            i === props.progress.id
              ? props.progress.completed
              : i < props.progress.id
                ? 1
                : 0
          }
        />
      ))
    }
  </StyledProgressArray>
)

ProgressArray.propTypes = {
  largo: PropTypes.array,
  progress: PropTypes.object
};

export default ProgressArray