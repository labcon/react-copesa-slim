import React from 'react'
import PropTypes from 'prop-types'
import Img from "gatsby-image"
import styled from 'styled-components'
import { darken } from 'polished'
import Share from './Sharer'

const SpecialBlock = ({ firstname, lastname, jobtitle, character, children, imgentrevistado, imgpersonaje, bgcolor, textcolor}) => (
  <SpecialBlockWrapper bgcolor={bgcolor} textcolor={textcolor} className="specialblock">
    <div className="specialblock-inner">
      <div className="col col1">
        <div className="inner">
          <div className="interviewed">
            <div className="interviewed-image">
              {imgentrevistado && imgentrevistado.localFile &&
                <Img fluid={imgentrevistado.localFile.childImageSharp.fluid} />
              }
            </div>
            <div className="interviewed-title">
              <h2> {firstname} <strong className="interviewed-line">{lastname}</strong> <small>{jobtitle}</small></h2>
            </div>
          </div>
          <div className="character">
            <div className="character-image">
              { imgpersonaje && imgpersonaje.localFile &&
                <Img className="character-imgtag" fluid={imgpersonaje.localFile.childImageSharp.fluid} />
              }
              <h3 className="character-title">
                <span dangerouslySetInnerHTML={{ __html: character }} />
              </h3>
            </div>
          </div>
        </div>
      </div>
      <div className="col col2">
        <div className="inner">
          <div className="interviewed-title">
            <div className="special-social">
              <Share
                socialConfig={{
                  twitterHandle: '@latercera',
                  config: {
                    url: `https://especiales.latercera.com/personaje-ficcion-verano`,
                    title: `${firstname} ${lastname} la pasaría con ${character.replace(/(<([^>]+)>)/ig, "")}`,
                  }
                }}
              />
            </div>
            <h2 className="title-question">¿Con qué personaje de ficción pasarías el verano?</h2>
          </div>
          <div className="interviewed-text">
            {children}
          </div>
        </div>
      </div>
    </div>
  </SpecialBlockWrapper>
)

const SpecialBlockWrapper = styled.section`
  min-height: 100vh;
  background-color: ${props => props.bgcolor ? props.bgcolor : '#0f0'};
  color: ${props => props.textcolor ? props.textcolor : 'white'};
  .col2 {
    background-color: ${props => props.bgcolor ? darken(0.1, props.bgcolor) : darken(0.1, '#0f0')};
  }
  h2, h3{
    color: ${props => props.textcolor ? props.textcolor : 'white'};
    .interviewed-line{
      border-color: ${props => props.textcolor ? props.textcolor : 'white'};
    }
  }
`

SpecialBlock.propTypes = {
  children: PropTypes.node.isRequired,
}

export default SpecialBlock
