import React from "react"
import Verano from "./verano"

const SpecialJumbo = () => (
  <section className="specialJumbo">
    <div className="specialJumboInner">
      <div className="col1">
        <h1>
          <span>
            ¿Con qué personaje <br /> de ficción<br /> pasarías el
        </span>
          <div className="ttl-verano">
            <Verano />
          </div>
        </h1>
      </div>
      <div className="col2">
        <h5>Por: Rosario Mendía y Carlos Pérez</h5>
        <p>El sol pega fuerte, las playas se llenan. Se vive con mayor relajo. Hay tiempo incluso para un ejercicio de imaginación: le propusimos a 20 personas responder la pregunta ¿Con qué personaje de película, serie o literatura pasarías tu verano? El resultado fue diverso. Una pretemporada con los jugadores de Barrabases; un viaje en auto con The Dude, protagonista de El gran Lebowski; unas vacaciones con Serena, de la serie japonesa Sailor Moon; o conversaciones con Luke Skywalker, de Star Wars.</p>
      </div>
    </div>
  </section>
)

export default SpecialJumbo

