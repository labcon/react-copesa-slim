# lt-ui

> UI LT portions

[![NPM](https://img.shields.io/npm/v/lt-ui.svg)](https://www.npmjs.com/package/lt-ui) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save lt-ui
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'lt-ui'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [Álex Acuña Viera &lt;kyuumeitai@gmail.com&gt;](https://github.com/Álex Acuña Viera &lt;kyuumeitai@gmail.com&gt;)
