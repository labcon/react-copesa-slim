# lt-stories

> Stories component for LT

[![NPM](https://img.shields.io/npm/v/lt-stories.svg)](https://www.npmjs.com/package/lt-stories) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save lt-stories
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'lt-stories'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [Álex Acuña Viera &lt;kyuumeitai@gmail.com&gt;](https://github.com/Álex Acuña Viera &lt;kyuumeitai@gmail.com&gt;)
