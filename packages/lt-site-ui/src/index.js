import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'

import Social from './components/social/social'
import Logo from './components/logo/logo'
import Digitalpaper from './components/digitalpaper/digitalpaper'
import Nav from './components/nav/nav'
import Search from './components/search/search'
import Menu from './components/menu/menu'

library.add(fab, fas)

export { Social, Logo, Digitalpaper, Nav, Search, Menu }
export default { Social, Logo, Digitalpaper, Nav, Search, Menu }
