import styled from 'styled-components'

const Container = styled.div`
  width: 100%;
  overflow: hidden;
  >a {
    text-decoration: none;
    display: grid;
    grid-template-columns: repeat(2, 1fr);
  }
  img{
    max-width: 100%;
  }
`

const Col1 = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  text-align: right;
  h1{
    text-transform: uppercase;
    font-weight: 300;
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
    max-width: 120px;
    color: rgba(0,0,0,0.5);
    line-height: 1.1rem;
  }
`

const Col2 = styled.div`
  position: relative;
  .ea-app {
    position: absolute;
    right: 35px;
    top: 15px;
    width: 50px;
    height: 50px;
    border-radius: 10px;
    overflow: hidden;
    display: block;
    z-index: 6;
    box-shadow: 0 0 10px rgba(0, 0, 0, .25);
  }
  .over-shadow {
    height: 100%;
    width: calc(100% - 40px);
    background-image: url(https://s2.latercera.com/wp-content/themes/gnuble/img/recursos/over-paper.png);
    left: 10px;
    position: absolute;
    top: 10px;
    z-index: 5;
  }
  .news {
    display: block;
    padding-top: 92.8%;
    position: absolute;
    background-size: 100%;
    height: 100%;
    width: calc(100% - 40px);
    left: 10px;
    top: 10px;
    &:before {
      top: 6px;
      left: 6px;
      height: calc(100% - 20px);
    }
    img {
      width: 100%;
      position: absolute;
      top: 0;
      left: 0;
      z-index: 3;
    }
  }
`

export {
  Container, Col1, Col2
}

export default {
  Container, Col1, Col2
}
