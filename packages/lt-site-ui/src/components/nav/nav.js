import React, { Component } from 'react'
import propTypes from 'prop-types'
import Container from './nav.css.js'

class Nav extends Component {
  render() {
    const linkItems = this.props.links.map((link, index) =>
      <a key={index} href={link.url}>{link.text}</a>
    )
    return (
      <Container>
        <nav>
          {linkItems}
        </nav>
      </Container>
    )
  }
}

Nav.propTypes = {
  links: propTypes.array
}

Nav.defaultProps = {
  links: [
    {
      url: 'https://www.latercera.com',
      text: 'Inicio',
      icon: 'home'
    },
    {
      url: 'https://www.latercera.com/la-tercera-pm',
      text: 'La Tercera PM'
    },
    {
      url: 'https://www.latercera.com/canal/pulso',
      text: 'Pulso'
    },
    {
      url: 'https://www.latercera.com/canal/politica',
      text: 'Política'
    },
    {
      url: 'https://www.latercera.com/canal/nacional',
      text: 'Nacional'
    },
    {
      url: 'https://www.latercera.com/canal/mundo',
      text: 'Mundo'
    },
    {
      url: 'https://www.latercera.com/canal/opinion/',
      text: 'Opinión'
    },
    {
      url: 'https://www.latercera.com/canal/tendencias/',
      text: 'Tendencias'
    },
    {
      url: 'https://www.latercera.com/canal/cultura/',
      text: 'Cultura'
    },
    {
      url: 'https://www.latercera.com/canal/entretencion/',
      text: 'Entretención'
    },
    {
      url: 'https://www.latercera.com/canal/reportajes/',
      text: 'Reportajes'
    },
    {
      url: 'https://www.latercera.com/canal/el-deportivo/',
      text: 'El Deportivo'
    },
    {
      url: 'https://www.latercera.com/canal/que-pasa/',
      text: 'Qué Pasa'
    },
    {
      url: 'http://edition.pagesuite.com/html5/reader/production/default.aspx?pubname=&amp;pubid=33084897-397a-48cc-b3c0-3ce1ec447137',
      text: 'Edición Impresa'
    },
    {
      url: 'https://www.latercera.com/canal/la-tercera-tv/',
      text: 'La Tercera TV'
    }
  ]
}

export default Nav
