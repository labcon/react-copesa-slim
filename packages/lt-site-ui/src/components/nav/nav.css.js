import styled from 'styled-components'
import colors from '../../styles/colors.css'

const Container = styled.div`
  white-space: nowrap;
  height: 40px;
  display: flex;
  align-items: center;
  nav{
    display: inline-block;
    a{
      display: inline-block;
      padding: 5px;
      color: ${colors.gris};
      text-decoration: none;
      text-transform: uppercase;
      font-family: 'Open Sans', Arial, sans-serif;
      font-size: 13.5px;
    }
  }
`

export default Container
