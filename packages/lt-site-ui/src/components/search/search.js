import React, { Component } from 'react'
import Container from './search.css.js'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Search extends Component {
  render() {
    return (
      <Container>
        <FontAwesomeIcon icon={['fas', 'search']} />
      </Container>
    )
  }
}

export default Search
