import styled from 'styled-components'
import colors from '../../styles/colors.css'

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  a{
    color: ${colors.azul};
    display: flex;
    width: 30px;
    height: 30px;
    border: 1px solid ${colors.grisclaro};
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    position: relative;
    &:after{
      transition: all 0.3s;
      position: absolute;
      content: '';
      background-color: transparent;
      width: 30px;
      height: 30px;
      display: block;
      transform: scale(0.1);
      z-index: -1;
      border-radius: 50%;
    }
    &:hover{
      &:after{
        transform: scale(1);
        background-color: ${colors.grisclaro};
      }
    }
  }
`
export default Container
