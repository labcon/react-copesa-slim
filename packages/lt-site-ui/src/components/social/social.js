import React from 'react'
import Container from './social.css.js'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Social = () => (
  <Container>
    <a href='https://twitter.com/latercera' target='_blank' rel='noopener noreferrer'>
      <FontAwesomeIcon icon={['fab', 'twitter']} />
    </a>
    <a href='https://www.facebook.com/laterceracom' target='_blank' rel='noopener noreferrer'>
      <FontAwesomeIcon icon={['fab', 'facebook-f']} />
    </a>
    <a href='https://flipboard.com/@latercera' target='_blank' rel='noopener noreferrer'>
      <FontAwesomeIcon icon={['fab', 'flipboard']} />
    </a>
    <a href='https://www.instagram.com/laterceracom/' target='_blank' rel='noopener noreferrer'>
      <FontAwesomeIcon icon={['fab', 'instagram']} />
    </a>
    <a href='https://www.youtube.com/user/latercera' target='_blank' rel='noopener noreferrer'>
      <FontAwesomeIcon icon={['fab', 'youtube']} />
    </a>
  </Container>
)

export default Social
