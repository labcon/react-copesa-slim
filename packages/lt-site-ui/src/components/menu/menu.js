import React from 'react'
import Container from './menu.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Menu = () => (
  <Container>
    <FontAwesomeIcon icon={['fas', 'bars']} />
  </Container>
)

export default Menu
