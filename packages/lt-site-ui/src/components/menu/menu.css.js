import styled from 'styled-components'
import colors from '../../styles/colors.css'

const Container = styled.div`
  display: block;
  color: ${colors.gris}
`

export default Container
