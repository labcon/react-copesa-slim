import React, {Component} from 'react'
import propTypes from 'prop-types'
import Svgs from 'copesa-logos'
import Container from './logo.css.js'
class Logo extends Component {
  render() {
    const LogoRender = Svgs[this.props.brand]
    return (
      <Container>
        <LogoRender />
      </Container>
    )
  }
}

Logo.propTypes = {
  brand: propTypes.oneOf([
    'Latercera',
    'Beethoven',
    'Biut',
    'Diarioconcepcion',
    'Duna',
    'Glamorama',
    'Grupocopesa',
    'GrupocopesaAlt',
    'Icarito',
    'LaboratorioDeContenidosDeMarca',
    'Lacuarta',
    'LahoraTubuenanoticia',
    'LtEldeportivo',
    'LtFinde',
    'LtNegocios',
    'LtPulso',
    'LtReportajes',
    'LtTendencias',
    'Masdeco',
    'MediosDigitalesGrupocopesa',
    'Mouse',
    'Mtonline',
    'Paula',
    'Quepasa',
    'Radiodisney',
    'Zero',
    'ZeroAlt'
  ])
}

Logo.defaultProps = {
  brand: 'Latercera'
}

export default Logo
