import React, { Component } from 'react'

import {Social, Logo, Digitalpaper, Nav, Search, Menu} from 'lt-site-ui'

export default class App extends Component {
  render () {
    return (
      <div>
        <Social />
        <Logo />
        <Digitalpaper />
        <Nav />
        <Search />
        <Menu />
      </div>
    )
  }
}
