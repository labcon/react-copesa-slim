# lt-site-ui

> lt site ui

[![NPM](https://img.shields.io/npm/v/lt-site-ui.svg)](https://www.npmjs.com/package/lt-site-ui) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save lt-site-ui
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'lt-site-ui'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [kyuumeitai](https://github.com/kyuumeitai)
