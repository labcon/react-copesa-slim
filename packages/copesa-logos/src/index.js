import Latercera from './logos/Latercera'
import Beethoven from './logos/Beethoven'
import Biut from './logos/Biut'
import Diarioconcepcion from './logos/Diarioconcepcion'
import Duna from './logos/Duna'
import Glamorama from './logos/Glamorama'
import Grupocopesa from './logos/Grupocopesa'
import GrupocopesaAlt from './logos/GrupocopesaAlt'
import Icarito from './logos/Icarito'
import LaboratorioDeContenidosDeMarca from './logos/LaboratorioDeContenidosDeMarca'
import Lacuarta from './logos/Lacuarta'
import LahoraTubuenanoticia from './logos/LahoraTubuenanoticia'
import LtEldeportivo from './logos/LtEldeportivo'
import LtFinde from './logos/LtFinde'
import LtNegocios from './logos/LtNegocios'
import LtPulso from './logos/LtPulso'
import LtReportajes from './logos/LtReportajes'
import LtTendencias from './logos/LtTendencias'
import Masdeco from './logos/Masdeco'
import MediosDigitalesGrupocopesa from './logos/MediosDigitalesGrupocopesa'
import Mouse from './logos/Mouse'
import Mtonline from './logos/Mtonline'
import Paula from './logos/Paula'
import Quepasa from './logos/Quepasa'
import Radiodisney from './logos/Radiodisney'
import Zero from './logos/Zero'
import ZeroAlt from './logos/ZeroAlt'

const Logos = {
  Latercera,
  Beethoven,
  Biut,
  Diarioconcepcion,
  Duna,
  Glamorama,
  Grupocopesa,
  GrupocopesaAlt,
  Icarito,
  LaboratorioDeContenidosDeMarca,
  Lacuarta,
  LahoraTubuenanoticia,
  LtEldeportivo,
  LtFinde,
  LtNegocios,
  LtPulso,
  LtReportajes,
  LtTendencias,
  Masdeco,
  MediosDigitalesGrupocopesa,
  Mouse,
  Mtonline,
  Paula,
  Quepasa,
  Radiodisney,
  Zero,
  ZeroAlt
}

export default Logos
export {
  Latercera,
  Beethoven,
  Biut,
  Diarioconcepcion,
  Duna,
  Glamorama,
  Grupocopesa,
  GrupocopesaAlt,
  Icarito,
  LaboratorioDeContenidosDeMarca,
  Lacuarta,
  LahoraTubuenanoticia,
  LtEldeportivo,
  LtFinde,
  LtNegocios,
  LtPulso,
  LtReportajes,
  LtTendencias,
  Masdeco,
  MediosDigitalesGrupocopesa,
  Mouse,
  Mtonline,
  Paula,
  Quepasa,
  Radiodisney,
  Zero,
  ZeroAlt
}
